Core = require("core")
SerenityState = Core.SerenityState

# Screeps tick function
module.exports.loop = ->
  Memory.serenity_state = new SerenityState() unless Memory.serenity_state

  # Deserializing Serenity State
  Game.serenity = new SerenityState(Memory.serenity_state)

  Game.serenity.preTick()
  Game.serenity.tick()
  Game.serenity.postTick()

  # Serializing new State
  Memory.serenity_state = Game.serenity

  return
