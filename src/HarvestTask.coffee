Task = require("Task")
GoNear = require("GoNear")

class HarvestTask extends Task
  constructor: ({owner, serializedData}) ->
    super({
      owner: owner
      className: "HarvestTask"
      serializedData: serializedData
    })

    if serializedData? then return

    @sourceId = owner.pos.findClosestByPath(FIND_SOURCES).id
  
  getSource: ->
    return Game.getObjectById(@sourceId)
  preTick: ->
    super()
    if @subtasksQueue.length == 0
      sourceObj = @getSource()
      if sourceObj?
        @subtasksQueue = [new GoNear({
          superTask: this
          targetObject: sourceObj
        })]
        @subtasksQueue[0].preTick()
  
  tick: ->
    super()

module.exports = HarvestTask
