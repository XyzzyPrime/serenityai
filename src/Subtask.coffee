MemoryObject = require('core').MemoryObject

class Subtask extends MemoryObject
  constructor: ({superTask, className, serializedData}) ->
    super(serializedData, className)
    
    unless serializedData?
      # Default constructor
      @superTaskId = superTask.taskId
  
  getSuperTask: ->
    return Game.serenity.tasks[@superTaskId]
  
  isComplete: ->
    return true
  
  preTick: ->

  tick: ->

  postTick: ->

module.exports = Subtask
